const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dotenv = require('dotenv');

dotenv.config();

const app = express();

var corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));

const PROFILE = process.env.PROFILE || "dev";
const db = require("./app/model");
if (PROFILE === "dev") {
    db.sequelize.sync({
        force: true
    }).then(() => {
        console.log("Drop and re-sync db.");
    });
} else {
    db.sequelize.sync();
}


const home = require("./app/domain/home/home.route");
const consumer = require("./app/domain/consumer/consumer.route");
const task = require("./app/domain/task/task.route");
app.use('/api/home', home);
app.use('/api/consumer', consumer);
app.use('/api/task', task);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
