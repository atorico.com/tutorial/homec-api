
const DB_HOST = process.env.POSTGRES_HOST || "localhost";
const DB_PORT = process.env.POSTGRES_PORT || 5432;
const DB_USER = process.env.POSTGRES_USER || "postgres";
const DB_PASS = process.env.POSTGRES_PASSWORD || "postgres";
const DB = process.env.POSTGRES_DB || "postgres";
console.log(process.env.POSTGRES_PASSWORD);
console.log(DB_PASS);

module.exports = {
  HOST: DB_HOST,
  PORT: DB_PORT,
  USER: DB_USER,
  PASSWORD: DB_PASS,
  DB: DB,
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
