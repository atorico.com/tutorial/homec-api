const extractUser = (req) => {
    return "test-consumer";
};

exports.extractUser = extractUser;
exports.location = (req, urlSuffix) => {
    const baseUrl = req.baseUrl;
    let protocol = req.protocol;
    let host = req.headers.host;
    const forwardedHost = req.headers['x-forwarded-host'];
    if ( forwardedHost ){
        host = forwardedHost;
    }
    const forwardedProto = req.headers['x-forwarded-proto'];
    if ( forwardedProto ){
        protocol = forwardedProto;
    }
    return protocol + "://" + host + baseUrl;
};

