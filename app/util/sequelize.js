const extractUser = (req) => {
    return "test-consumer";
};
const logSourceInfo = (req) => {
    return {
        _sourceType: 'users',
        _sourceId: extractUser(req)
    };
};

exports.transaction = (req, sequelize, oModel, sSequelizeMethod, ...aParams) => {
    if (!req || !req.body || !req.params) {
        throw new Errow("request parameter is required");
    }
    const oSourceInfo = logSourceInfo(req);
    if (!oSourceInfo || !oSourceInfo._sourceType) {
        throw new Errow("oSourceInfo parameter must be defined and contain the _sourceType key and optionally the _sourceId key");
    }
    if (!oModel || (!oModel.constructor && !oModel.constructor.tableName)) {
        throw new Error(`${oModel} is not a sequelize model`);
    }
    if (typeof oModel[sSequelizeMethod] !== 'function') {
        throw new Error(`${sSequelizeMethod} is not a function on the model`);
    }
    const sMethod = oModel[sSequelizeMethod].name;
    return sequelize.transaction(t => {
        let options = {
            transaction: t,
        };
        Object.assign(options, oSourceInfo);
        if (sMethod.indexOf("create") === 0 || sMethod.indexOf("update") === 0) {
            if (aParams.length === 2) {
                return oModel[sSequelizeMethod](aParams[0], Object.assign(options, aParams[1]));
            }
            return oModel[sSequelizeMethod](aParams[0], options);
        } else {
            if (aParams.length === 1) {
                return oModel[sSequelizeMethod](Object.assign(options, aParams[0]));
            }
            return oModel[sSequelizeMethod](options);
        }
    })
    .catch(err => {
        console.error(err);
        throw new Error(err.errors[0].type + ": " + err.errors[0].message);
    });
};
