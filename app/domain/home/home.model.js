const { Model } = require("sequelize");
const hooks = require("../auditlog/auditlog.hooks");

class Home extends Model {

}
module.exports = (sequelize, Sequelize) => {
    Home.init({
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        uuid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'home',
        hooks: hooks
    });

    return Home;
};
