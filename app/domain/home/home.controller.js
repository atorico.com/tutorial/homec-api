const {
    location,
    extractUser
} = require("../../util");
const service = require("./home.service");

// Create and Save a new Home
exports.create = (req, res) => {
    // Validate request
    if (!req.body.title) {
        res.status(400).send({
            message: "title can not be empty!"
        });
        return;
    }
    const home = {
        title: req.body.title,
        description: req.body.description,
        assigned_ref: req.body.assigned_ref,
        done: req.body.done || false,
        extra: req.body.extra
        //creator_ref: req.body.creator_ref, get from request
    };
    return service.create(req, home)
        .then(data => {
            res.location(location(req) + "/" + data.uuid);
            res.status(201).send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Home."
            });
        });
};

// Find a single Home with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    service.findOne(req, id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Home with uuid=" + id
            });
        });
};

// Update a Home by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    service.update(req, id, home)
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Home was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Home with uuid=${id}. Maybe Home was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Home with uuid=" + id
            });
        });
};

// Delete a Home with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    service.delete(req, id)
        .then(num => {
            res.send({
                message: "Home was deleted successfully!"
            });
            return num;
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Home with uuid=" + id
            });
        });
};

// assign a home to a consumer
exports.addMember = (req, res) => {
    const id = req.params.id;
    if (!id) {
        res.status(400).send({
            message: "id is required!"
        });
        return;
    }
    const consumerRef = req.params.consumer;
    if (!consumerRef) {
        res.status(400).send({
            message: "consumer is required!"
        });
        return;
    }
    service.addMember(req, id, consumerRef)
        .then(num => {
            if (num && num.homeId && num.memberId) {
                res.send({
                    message: `Member was successfully added to Home: ${consumerRef}.`
                });
            } else {
                res.send({
                    message: `Cannot add Member, consumer=${consumerRef}, to the Home with uuid=${id}!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Home with uuid=" + id
            });
        });
};
// assign a home to a consumer
exports.removeMember = (req, res) => {
    const id = req.params.id;
    if (!id) {
        res.status(400).send({
            message: "id is required!"
        });
        return;
    }
    const consumerRef = req.params.consumer;
    if (!consumerRef) {
        res.status(400).send({
            message: "consumer is required!"
        });
        return;
    }
    service.removeMember(req, id, consumerRef)
        .then(num => {
            if (num) {
                res.send({
                    message: `Member was successfully removed from Home: ${consumerRef}.`
                });
            } else {
                res.send({
                    message: `Cannot remove Member, consumer=${consumerRef}, from the Home with uuid=${id}!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Home with uuid=" + id
            });
        });
};

// Find a single Homes members
exports.findMembers = (req, res) => {
    const id = req.params.id;
    service.findOne(req, id)
        .then(home => {
            return home.getConsumers();
        })
        .then(consumers => {
            return consumers.map(consumer => {
                const c = {};
                Object.assign(c, consumer.dataValues);
                delete c.home_member;
                return c;
            });
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Members for Home with uuid=" + id
            });
        });
};
