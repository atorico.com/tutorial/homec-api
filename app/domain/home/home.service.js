const {
    db,
    sequelize
} = require("../../model");
const Home = db.home;
const Consumer = db.consumer;
const HomeMember = db.homeMember
const {
    extractUser
} = require("../../util");
const {
    transaction
} = require("../../util/sequelize");

// Create and Save a new Home
exports.create = (req, home) => {
    // Validate request
    if (!home.title) {
        throw new Error("Home requires a title");
    }
    return transaction(req, sequelize, Home, "create", home);
};

// Find a single Home with an id
exports.findOne = (req, id) => {
    if (!id) {
        throw new Error("id is required");
    }
    return transaction(req, sequelize, Home, "findOne", {
        where: {
            uuid: id
        }
    });
};

// Update a Home by the id in the request
exports.update = (req, id, home) => {
    if (!id) {
        throw new Error("id is required");
    }
    return transaction(req, sequelize, Home, "update", home, {
        where: {
            uuid: id
        }
    });
};

// Delete a Home with the specified id in the request
exports.delete = (req, id) => {
    if (!id) {
        throw new Error("id is required");
    }
    return Home.findOne({
        where: {
            uuid: id
        }
    }).then(home => {
        return transaction(req, sequelize, home, "destroy");
    });
};

// assign a home to a consumer
exports.addMember = (req, id, consumerRef) => {
    if (!id) {
        throw new Error("id is required");
    }
    if (!consumerRef) {
        throw new Error("consumer is required!");
    }

    return sequelize.transaction(t => {
            return Consumer.findOne({
                    where: {
                        uuid: consumerRef
                    },
                    transaction: t,
                    _sourceType: 'users',
                    _sourceId: extractUser(req)
                })
                .then(consumer => {
                    return Home.findOne({
                            where: {
                                uuid: id
                            },
                            transaction: t,
                            _sourceType: 'users',
                            _sourceId: extractUser(req)
                        })
                        .then(home => {
                            return HomeMember.findAll({
                                    where: {
                                        homeId: home.dataValues.id
                                    }
                                }, {
                                    transaction: t,
                                    _sourceType: 'users',
                                    _sourceId: extractUser(req)
                                })
                                .then(homeMembers => {
                                    const homeMember = homeMembers.find(hm => {
                                        return hm.memberId === consumer.dataValues.id;
                                    });
                                    if (!homeMember) {
                                        return HomeMember.create({
                                            homeId: home.dataValues.id,
                                            memberId: consumer.dataValues.id
                                        }, {
                                            transaction: t,
                                            _sourceType: 'users',
                                            _sourceId: extractUser(req)
                                        });
                                    } else {
                                        throw new Error("Home already has Member");
                                    }
                                });
                        });
                });
        })
        .catch(err => {
            console.error(err);
            throw new Error(err.errors[0].type + ": " + err.errors[0].message);
        });
};

// remove a consumer as member of a home
exports.removeMember = (req, id, consumerRef) => {
    if (!id) {
        throw new Error("id is required");
    }
    if (!consumerRef) {
        throw new Error("consumer is required!");
    }

    return sequelize.transaction(t => {
            return Consumer.findOne({
                    where: {
                        uuid: consumerRef
                    },
                    transaction: t,
                    _sourceType: 'users',
                    _sourceId: extractUser(req)
                })
                .then(consumer => {
                    return Home.findOne({
                            where: {
                                uuid: id
                            },
                            transaction: t,
                            _sourceType: 'users',
                            _sourceId: extractUser(req)
                        })
                        .then(home => {
                            return HomeMember.findOne({
                                where: {
                                    homeId: home.dataValues.id,
                                    memberId: consumer.dataValues.id
                                }
                            }, {
                                transaction: t,
                                _sourceType: 'users',
                                _sourceId: extractUser(req)
                            });
                        });
                })
                .then(homeMember => {
                    return homeMember.destroy({
                        transaction: t,
                        _sourceType: 'users',
                        _sourceId: extractUser(req)
                    });
                });
        })
        .catch(err => {
            console.error(err);
            throw new Error(err.errors[0].type + ": " + err.errors[0].message);
        });
};
