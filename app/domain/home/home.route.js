const router = require("express").Router();
const home = require("./home.controller.js");

// Create a new Home
router.post("/", home.create);

// Retrieve a single Home with id
router.get("/:id", home.findOne);

// Update a Home with id
router.put("/:id", home.update);

// Delete a Home with id
router.delete("/:id", home.delete);

// List Consumers belonging to a Home
router.get("/:id/member", home.findMembers);

// Add a Consumer to a Home
router.put("/:id/member/:consumer", home.addMember);

// Remove a Consumer from a Home
router.delete("/:id/member/:consumer", home.removeMember);

module.exports = router;
