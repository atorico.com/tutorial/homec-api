const assert = require("assert");
const _ = require("lodash");

const saveAuditLog = (action, model, options) => {
    // Verify we are being run in a transaction
    assert(options.transaction, 'All create/update/delete actions must be run in a transaction to ' +
        'prevent orphaned AuditLogs or connected models on save. ' +
        'Please add a transaction to your current "' + action + '" request');

    const {
        AuditLog
    } = require("../../model");

    // Resolve our model's constructor
    const constructor = model.constructor;
    const pks = constructor.primaryKeyAttributes;
    const rowId = pks
        .map(pk => model[pk])
        .join('__');
    var auditLog = AuditLog.build({
        source_type: options._sourceType, // 'server', 'users'
        source_id: options._sourceId, // NULL (server), user.id
        table_name: constructor.tableName,
        table_row_id: rowId,
        action: action,
        timestamp: new Date(),
        // https://github.com/sequelize/sequelize/blob/v3.25.0/lib/instance.js#L86-L87
        // https://github.com/sequelize/sequelize/blob/v3.25.0/lib/instance.js#L417-L433
        previous_values: _.pick(model._previousDataValues, options.defaultFields),
        current_values: _.pick(model.dataValues, options.defaultFields),
        transaction_id: options.transaction ? options.transaction.id : null
    });
    return auditLog.save({
        transaction: options.transaction
    });
};
module.exports = {
    // DEV: We don't support bulk actions due to not knowing previous/current info for models
    beforeBulkCreate: function() {
        throw new Error('Audit logging not supported for bulk creation; either add support or use `create` directly');
    },
    beforeBulkUpdate: function(model, options) {
        console.warn('Audit logging not supported for bulk updates; either add support or use `create` directly');
    },
    beforeBulkDestroy: function() {
        throw new Error('Audit logging not supported for bulk deletion; either add support or use `create` directly');
    },
    afterCreate: function(model, options) {
        return saveAuditLog('create', model, options);
    },
    afterUpdate: function(model, options) {
        return saveAuditLog('update', model, options);
    },
    afterDestroy: function(model, options) {
        return saveAuditLog('delete', model, options);
    }
}
