const { Model } = require("sequelize");
const hooks = require("../auditlog/auditlog.hooks");

class Consumer extends Model {

}
module.exports = (sequelize, Sequelize) => {
    return Consumer.init({
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        uuid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true
        },
        sid: {
            type: Sequelize.TEXT,
            allowNull: false,
            unique: true
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        activated: {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        info: {
            type: Sequelize.JSON
        },
        contact: {
            type: Sequelize.JSON
        },
        extra: {
            type: Sequelize.JSON
        }
    }, {
        sequelize,
        modelName: 'consumer',
        hooks: hooks
    });
};
