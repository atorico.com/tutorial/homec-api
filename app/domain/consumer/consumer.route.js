const router = require("express").Router();

const consumer = require("./consumer.controller.js");

// Create a new Tutorial
router.post("/", consumer.create);

// Retrieve all Tutorials
router.get("/", consumer.findAll);

// Retrieve a single Tutorial with id
router.get("/:id", consumer.findOne);

// Update a Tutorial with id
router.put("/:id", consumer.update);

// Delete a Tutorial with id
router.delete("/:id", consumer.delete);

// Get the Homes for the Consumer  with id
router.get("/:id/home", consumer.findHomes);

module.exports = router;
