const {
    db,
    sequelize
} = require("../../model");
const Consumer = db.consumer;
const {
    transaction
} = require("../../util/sequelize");


// Create and Save a new Consumer
exports.create = (req, consumer) => {
    // Validate request
    if (!consumer.sid) {
        throw new Error("sid can not be empty!");
    }
    if (!consumer.email) {
        throw new Error("Email can not be empty!");
    }
    if (!consumer.name) {
        throw new Error("Name can not be empty!");
    }
    return transaction(req, sequelize, Consumer, "create", consumer);
};
// Retrieve all Consumers from the database.
exports.findAll = (req) => {
    var condition = {
        activated: true
    };
    return transaction(req, sequelize, Consumer, "findAll", {
        where: condition
    });
};

// Find a single Consumer with an id
exports.findOne = (req, id) => {
    return transaction(req, sequelize, Consumer, "findOne", {
        where: {
            uuid: id
        }
    });
};

// Update a Consumer by the id in the request
exports.update = (req, id, consumer) => {
    return transaction(req, sequelize, Consumer, "update", consumer, {
        where: {
            uuid: id
        }
    });
};

// Delete a Consumer with the specified id in the request
exports.delete = (req, id) => {
    if (!id) {
        throw new Error("id is required");
    }
    return Consumer.findOne({
            where: {
                uuid: id
            }
        })
        .then(consumer => {
            return transaction(req, sequelize, consumer, "destroy");
        });
};
