const {
    location
} = require("../../util");

const service = require("./consumer.service");

// Create and Save a new Consumer
exports.create = (req, res) => {
    // Validate request
    if (!req.body.sid) {
        res.status(400).send({
            message: "sid can not be empty!"
        });
        return;
    }
    if (!req.body.email) {
        res.status(400).send({
            message: "Email can not be empty!"
        });
        return;
    }
    if (!req.body.name) {
        res.status(400).send({
            message: "Name can not be empty!"
        });
        return;
    }

    // Create a Consumer
    const consumer = {
        sid: req.body.sid,
        name: req.body.name,
        email: req.body.email,
    };

    // Save Consumer in the database
    service.create(req, consumer)
        .then(data => {
            res.location(location(req) + "/" + data.uuid);
            res.status(201).send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Consumer."
            });
        });
};

// Retrieve all Consumers from the database.
exports.findAll = (req, res) => {
    var condition = {
        activated: true
    };

    service.findAll(req, {
            where: condition
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving consumers."
            });
        });
};

// Find a single Consumer with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    service.findOne(req, id)
        .then(data => {
            if (data) {
                res.send(data);
                return data;
            }
            throw new Error("Not Found");
        })
        .catch(err => {
            if (err.message.indexOf("Not Found") === 0) {
                res.status(404).send({
                    message: "Not Found: Error retrieving Consumer with uuid=" + id
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Consumer with uuid=" + id
                });
            }
        });
};

// Update a Consumer by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    service.update(req, req.body, {
            where: {
                uuid: id
            }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Consumer was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Consumer with uuid=${id}. Maybe Consumer was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Consumer with uuid=" + id
            });
        });
};

// Delete a Consumer with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    service.delete(req, id)
        .then(num => {
            res.send({
                message: "Consumer was deleted successfully!"
            });
            return num;
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Consumer with uuid=" + id
            });
        });
};

// Find a single Consumers Homes
exports.findHomes = (req, res) => {
    const id = req.params.id;
    service.findOne(req, id)
        .then(consumer => {
            return consumer.getHomes();
        })
        .then(homes => {
            return homes.map(home => {
                const c = {};
                Object.assign(c, home.dataValues);
                delete c.home_member;
                return c;
            });
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Homes for Consumer with uuid=" + id
            });
        });
};
