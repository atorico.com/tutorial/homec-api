const router = require("express").Router();
const task = require("./task.controller.js");


// Create a new Task
router.post("/", task.create);

// Retrieve all finished tasks
router.get("/done", task.findAllDone);

// Retrieve all tasks
router.get("/all", task.findAll);

// Retrieve all unfinished Tasks
router.get("/", task.findAllToDo);

// Retrieve a single Task with id
router.get("/:id", task.findOne);

// Update a Task with id
router.put("/:id", task.update);

// Delete a Task with id
router.delete("/:id", task.delete);

// Set a Task with id to done
router.put("/:id/done", task.setDone);

// Assign a Task with id to a consumer
router.put("/:id/assign/:consumer", task.assign);

module.exports = router;
