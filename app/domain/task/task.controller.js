const {
    location,
    extractUser
} = require("../../util");
const service = require("./task.service");

// Create and Save a new Task
exports.create = (req, res) => {
    // Validate request
    if (!req.body.title) {
        res.status(400).send({
            message: "title can not be empty!"
        });
        return;
    }
    const task = {
        title: req.body.title,
        description: req.body.description,
        assigned_ref: req.body.assigned_ref,
        done: req.body.done || false,
        extra: req.body.extra
        //creator_ref: req.body.creator_ref, get from request
    };
    return service.create(req, task)
        .then(data => {
            res.location(location(req) + "/" + data.uuid);
            res.status(201).send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Task."
            });
        });
};

// Retrieve all done Tasks
exports.findAllDone = (req, res) => {
    service.findAllDone(req)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tasks."
            });
        });
};
// Retrieve all unfinished Tasks from the database.
exports.findAllToDo = (req, res) => {
    service.findAllToDo(req)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tasks."
            });
        });
};
// Retrieve all Tasks from the database.
exports.findAll = (req, res) => {
    service.findAll(req)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tasks."
            });
        });
};

// Find a single Task with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    service.findOne(req, id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Task with uuid=" + id
            });
        });
};

// Update a Task by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    service.update(req, id, task)
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Task was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Task with uuid=${id}. Maybe Task was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Task with uuid=" + id
            });
        });
};

// Delete a Task with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    service.delete(req, id)
        .then(num => {
            res.send({
                message: "Task was deleted successfully!"
            });
            return num;
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Task with uuid=" + id
            });
        });
};

// update finished status of task
exports.setDone = (req, res) => {
    const id = req.params.id;
    service.setDone(req, id)
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Task was successfully set to 'done'."
                });
            } else {
                res.send({
                    message: `Cannot set to 'done' the Task with uuid=${id}. Maybe Task was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Task with uuid=" + id
            });
        });
};
// assign a task to a consumer
exports.assign = (req, res) => {
    const id = req.params.id;
    if (!id) {
        res.status(400).send({
            message: "id is required!"
        });
        return;
    }
    const consumerRef = req.params.consumer;
    if (!consumerRef) {
        res.status(400).send({
            message: "consumer is required!"
        });
        return;
    }
    service.assign(req, id, consumerRef)
        .then(num => {
            if (num == 1) {
                res.send({
                    message: `Task was successfully assigned to consumer: ${consumerRef}.`
                });
            } else {
                res.send({
                    message: `Cannot assign the Task with uuid=${id} to consumer=${consumerRef}!`
                });
            }
        })
        .catch(err => {
            console.error(err);
            res.status(500).send({
                message: "Error updating Task with uuid=" + id
            });
        });
};
