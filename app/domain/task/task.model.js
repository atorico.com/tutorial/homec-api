const { Model } = require("sequelize");
const hooks = require("../auditlog/auditlog.hooks");

class Task extends Model {

}
module.exports = (sequelize, Sequelize) => {
    return Task.init({
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        uuid: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique: true
        },
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.TEXT
        },
        done: {
            type: Sequelize.BOOLEAN
        },
        assigned_ref: {
            type: Sequelize.STRING
        },
        creator_ref: {
            type: Sequelize.STRING
        },
        extra: {
            type: Sequelize.JSON
        }
    }, {
        sequelize,
        modelName: "task",
        hooks: hooks
    });
};
