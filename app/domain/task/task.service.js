const {
    db,
    sequelize
} = require("../../model");
const Task = db.task;
const Consumer = db.consumer;
const {
    extractUser
} = require("../../util");
const {
    transaction
} = require("../../util/sequelize");

// Create and Save a new Task
exports.create = (req, task) => {
    // Validate request
    if (!task.title) {
        throw new Error("Task requires a title");
    }
    return transaction(req, sequelize, Task, "create", task);
};

// Retrieve all done Tasks
exports.findAllDone = (req) => {
    var condition = {
        done: true
    };
    return transaction(req, sequelize, Task, "findAll", {
        where: condition
    });
};
// Retrieve all unfinished Tasks from the database.
exports.findAllToDo = (req) => {
    var condition = {
        done: false
    };
    return transaction(req, sequelize, Task, "findAll", {
        where: condition,
    });
};
// Retrieve all Tasks from the database.
exports.findAll = (req) => {
    return transaction(req, sequelize, Task, "findAll");
};

// Find a single Task with an id
exports.findOne = (req, id) => {
    if (!id) {
        throw new Error("id is required");
    }
    return transaction(req, sequelize, Task, "findOne", {
        where: {
            uuid: id
        }
    });
};

// Update a Task by the id in the request
exports.update = (req, id, task) => {
    if (!id) {
        throw new Error("id is required");
    }
    return transaction(req, sequelize, Task, "update", task, {
        where: {
            uuid: id
        }
    });
};

// Delete a Task with the specified id in the request
exports.delete = (req, id) => {
    if (!id) {
        throw new Error("id is required");
    }
    let condition = {
        uuid: id
    };
    return Task.findOne({
            where: condition
        })
        .then(task => {
            return transaction(req, sequelize, task, "destroy");
        });
};

// update finished status of task
exports.setDone = (req, id) => {
    if (!id) {
        throw new Error("id is required");
    }

    return sequelize.transaction(t => {
            return Task.findOne({
                    where: {
                        uuid: id
                    },
                    transaction: t,
                    _sourceType: 'users',
                    _sourceId: extractUser(req)
                })
                .then(task => {
                    return task.dataValues;
                })
                .then(task => {
                    task.done = true;
                    return Task.update(task, {
                        where: {
                            id: task.id
                        },
                        individualHooks: true,
                        transaction: t,
                        _sourceType: 'users',
                        _sourceId: extractUser(req)
                    });
                })
                .then(result => {
                    if (result && result.length && result.length > 0) {
                        return result[0];
                    }
                    throw new Error("result should be an array");
                });
        })
        .catch(err => {
            console.error(err);
            throw new Error(err.errors[0].type + ": " + err.errors[0].message);
        });
};
// assign a task to a consumer
exports.assign = (req, id, consumerRef) => {
    if (!id) {
        throw new Error("id is required");
    }
    if (!consumerRef) {
        throw new Error("consumer is required!");
    }

    return sequelize.transaction(t => {
            return Consumer.findOne({
                    where: {
                        uuid: consumerRef
                    },
                    transaction: t,
                    _sourceType: 'users',
                    _sourceId: extractUser(req)
                })
                .then(consumer => {
                    return Task.findOne({
                        where: {
                            uuid: id
                        },
                        transaction: t,
                        _sourceType: 'users',
                        _sourceId: extractUser(req)
                    });
                })
                .then(task => {
                    return task.dataValues;
                })
                .then(task => {
                    task.assigned_ref = consumerRef;
                    return Task.update(task, {
                        where: {
                            id: task.id
                        },
                        individualHooks: true,
                        transaction: t,
                        _sourceType: 'users',
                        _sourceId: extractUser(req)
                    });
                })
                .then(result => {
                    if (result && result.length && result.length > 0) {
                        return result[0];
                    }
                    throw new Error("result should be an array");
                });
        })
        .catch(err => {
            console.error(err);
            throw new Error(err.errors[0].type + ": " + err.errors[0].message);
        });
};
