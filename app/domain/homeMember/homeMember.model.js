const { Model } = require("sequelize");
const hooks = require("../auditlog/auditlog.hooks");

class HomeMember extends Model {

}
module.exports = (sequelize, Sequelize) => {
    HomeMember.init({
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        homeId: {
            type: Sequelize.INTEGER,
            unique: 'home_member'
        },
        memberId: {
            type: Sequelize.INTEGER,
            unique: 'home_member'
        }
    }, {
        sequelize,
        modelName: 'home_member',
        hooks: hooks
    });

    return HomeMember;
};
