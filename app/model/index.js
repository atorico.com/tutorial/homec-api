const dbConfig = require("../config/db.config.js");

const {
    Sequelize
} = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    port: dbConfig.PORT,
    dialect: dbConfig.dialect,
    define: {
        timestamps: true
    },
    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

const {
    AuditLog,
    SOURCE_USERS
} = require("../domain/auditlog/auditlog.model.js");

const auditlog = AuditLog(sequelize, Sequelize);
db.task = require("../domain/task/task.model.js")(sequelize, Sequelize);
db.consumer = require("../domain/consumer/consumer.model.js")(sequelize, Sequelize);
db.home = require("../domain/home/home.model.js")(sequelize, Sequelize);
db.homeMember = require("../domain/homeMember/homeMember.model.js")(sequelize, Sequelize);

const HomeMember = db.homeMember;
const Home = db.home;
const Consumer = db.consumer;

Home.belongsToMany(Consumer, {
    through: {
        model: HomeMember,
        unique: false
    },
    foreignKey: 'homeId'
});
Consumer.belongsToMany(Home, {
    through: {
        model: HomeMember,
        unique: false
    },
    foreignKey: 'memberId'
});

module.exports.db = db;
module.exports.sequelize = sequelize;
module.exports.AuditLog = auditlog;
module.exports.AL_SOURCE_USER = SOURCE_USERS;
